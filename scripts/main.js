//    1
// Document Object Model (Dom) it`s a model-document which introduce all things on a page in objeckts which can be changed.

 //   2
 // InnerHTML returns or sets the HTML content (including any tags) of an element, while innerText returns or sets only the 
 // text content of an element, without any HTML tags.

 //   3
 // To use an ellement in HTML We can use either (elem.getElementsByTagName(tag), elem.getElementsByClassName(className), 
 // document.getElementsByName(name) or querySelectorAll, the last one is better.

  


//    1 Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const allParagraphs = document.querySelectorAll('p');
allParagraphs.forEach((p) => {  
    p.style.background = "#ff0000";
});
//    2 Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. 
//   Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const idOptionsListElements = document.getElementById('optionsList');
console.log(idOptionsListElements);

const parentOFOptionsList = idOptionsListElements.closest("ul");
console.log(parentOFOptionsList);

const childNodes = idOptionsListElements.childNodes;
for (let i = 0; i < childNodes.length; i++) {
    const node = childNodes[i];
    if (node.nodeType === Node.ELEMENT_NODE) {
        console.log("Назва дочірнього вузла: " + node.nodeName + ", Тип дочірнього вузла: " + node.nodeType);
    }
};
//    3 Встановіть в якості контента елемента з класом testParagraph наступний параграф -
//  This is a paragraph 
const testParagraph = document.getElementById('testParagraph');
testParagraph.innerHTML = " This is a paragraph";

//    4  Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. 
// Кожному з елементів присвоїти новий клас nav-item.
const mainHeader = document.querySelectorAll('.main-header');
    for (var i = 0; i < mainHeader.length; i++) {
        console.log(mainHeader);
  }; 
  mainHeader.forEach(element => {
    element.classList.add('nav-item')

  });
//    5 Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

 const sectionTitleElements = document.querySelectorAll('.section-title');
    sectionTitleElements.forEach(element => {
        element.classList.remove('section-title');
    });


 







